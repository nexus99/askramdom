﻿using Practica4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica4
{
    public partial class Default : System.Web.UI.Page
    {
        static Trivia triv;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                triv = new Trivia();
                triv.preguntaRam();
                lblPregunta.Text = triv.trivia;
            }
            
        }

        protected void lkEnviar_Click(object sender, EventArgs e)
        {
            triv = new Trivia();
            triv.preguntaRam();
            lblPregunta.Text = triv.trivia;
            if (ddlRespuesta.SelectedValue == "3")
            {
                triv.reportar();
            }
            else
            {
                if (triv.respuesta == int.Parse(ddlRespuesta.SelectedValue))
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                            "alert('es correcto')", true);
                    triv.sumarCorrecta();
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                            "alert('es incorrecto imbeciiilll!!!!!!!')", true);
                    triv.sumarIncorrecta();
                }
            }

            
        }
    }
}