﻿using Practica4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Practica4
{
    public partial class panel : System.Web.UI.Page
    {
        Trivia triv;
        protected void Page_Load(object sender, EventArgs e)
        {
            triv = new Trivia();
            if (Session["id_usuario"] == null)
            {
                Response.Redirect("Default.aspx");
            }
            gvPreguntas.DataSource = triv.traerPreguntas();
            gvPreguntas.DataBind();

        }

        protected void lkCerrar_Click(object sender, EventArgs e)
        {
            Session["id_usuario"] = null;
            Response.Redirect("Default.aspx");
        }

        protected void btnAgregar_Click(object sender, EventArgs e)
        {
            triv.trivia = txtPregunta.Text.Trim();
            triv.id_usuario = (int) (Session["id_usuario"]);
            triv.respuesta = triv.asignarRespuesta(ddlRespuesta.SelectedItem.Text);
            triv.agregarPregunta();
            

        }

        protected void actualizar_Click(object sender, EventArgs e)
        {
            gvPreguntas.DataSource = triv.traerPreguntas();
        }

        protected void gvPreguntas_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gvPreguntas.SelectedRow;
            lblPregunta.Text = row.Cells[1].Text;
            lblID.Text = row.Cells[0].Text;

        }

        protected void lkEliminar_Click(object sender, EventArgs e)
        {
            if (lblPregunta.Text != "No hay selección")
            {
                triv.id_trivia = int.Parse(lblID.Text);
                triv.eliminarPregunta();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                        "alert('No se ha seleciona ninguna archivo')", true);
            }
        }

        protected void lkDesactivar_Click(object sender, EventArgs e)
        {
            if (lblPregunta.Text != "No hay selección")
            {
                triv.id_trivia = int.Parse(lblID.Text);
                triv.desactivarPregunta();
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "mensaje",
                        "alert('No se ha seleciona ninguna archivo')", true);
            }
        }
    }
}