﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Practica4.DAO;
using System.Data;
using System.Data.SqlClient;

namespace Practica4.Models
{
    public class Trivia: Conexion
    {
        public int id_trivia { set; get; }
        public string trivia { set; get; }
        public int respuesta { set; get; }
        public int id_usuario { set; get; }
        public int correctas { set; get; }
        public int incorrectas { set; get; }
        public int reportes { set; get; }
        public int total { set; get; }

        public int asignarRespuesta(string seleccion)
        {
            return (seleccion == "Verdadero") ? 1 : 2;
        }

        public int agregarPregunta()
        {
            try
            {
                string query = "INSERT INTO trivias(trivia,id_respuesta,id_usuario) VALUES(@trivia,@id_respuesta,@id_usuario)";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@trivia",trivia);
                cmd.Parameters.AddWithValue("@id_respuesta", respuesta);
                cmd.Parameters.AddWithValue("@id_usuario", id_usuario);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            
        }

        public DataTable traerPreguntas()
        {
            try
            {
                string query = "SELECT * FROM trivias T INNER JOIN respuestas R ON t.id_respuesta = R.id_respuesta INNER JOIN estatus E on e.id_estatus = t.id_estatus";
                return (Consulta(new SqlCommand(query)));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int sumarCorrecta()
        {
            try
            {
                correctas++;
                total = correctas + incorrectas;
                string query = "UPDATE trivias SET respuestasCorrectas = @r, total = @t WHERE id_trivia = @id_trivia";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@r",correctas);
                cmd.Parameters.AddWithValue("@t",total);
                cmd.Parameters.AddWithValue("@id_trivia",id_trivia);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int sumarIncorrecta()
        {
            try
            {
                incorrectas++;
                total = correctas + incorrectas;
                string query = "UPDATE trivias SET respuestasIncorrectas = @r, total = @t WHERE id_trivia = @id_trivia";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@r", incorrectas);
                cmd.Parameters.AddWithValue("@t", total);
                cmd.Parameters.AddWithValue("@id_trivia", id_trivia);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }


        public int eliminarPregunta()
        {
            try
            {
                string query = "DELETE FROM trivias WHERE id_trivia = @id_trivia";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_trivia", id_trivia);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public int desactivarPregunta()
        {
            try
            {
                string query = "UPDATE trivias SET id_estatus = 2 WHERE id_trivia =@id_trivia";
                SqlCommand cmd = new SqlCommand(query);
                cmd.Parameters.AddWithValue("@id_trivia", id_trivia);
                return Ejecutar(cmd);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public void preguntaRam()
        {
            try
            {
                string query = "SELECT * FROM trivias WHERE reportes < 3";
                DataTable trivias = Consulta(new SqlCommand(query));
                Random ran = new Random();
                DataRow fila = trivias.Rows[ran.Next(trivias.Rows.Count)];
                id_trivia = (int) (fila["id_trivia"]);
                trivia = (string) (fila["trivia"]);
                respuesta = (int) (fila["id_respuesta"]);
                correctas = (int)(fila["respuestasCorrectas"]);
                incorrectas = (int)(fila["respuestasIncorrectas"]);
                reportes = (int)(fila["reportes"]);
                total = (int)(fila["total"]);

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        public int reportar()
        {
            try
            {
                reportes++;                
                if (reportes > 3)
                {
                    string query = "UPDATE trivias SET reportes = @reportes, id_estatus = 3 WHERE id_trivia = @id_trivia";
                    SqlCommand cmd = new SqlCommand(query);
                    cmd.Parameters.AddWithValue("@id_trivia", id_trivia);
                    cmd.Parameters.AddWithValue("@reportes", reportes);
                    return Ejecutar(cmd);
                }
                else
                {
                    string query = "UPDATE trivias SET reportes = @reportes WHERE id_trivia = @id_trivia";
                    SqlCommand cmd = new SqlCommand(query);
                    cmd.Parameters.AddWithValue("@id_trivia", id_trivia);
                    cmd.Parameters.AddWithValue("@reportes", reportes);
                    return Ejecutar(cmd);
                }
                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
       
    }
}