﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Practica4.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>casa</title>
    <link href="Utelerias/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark ">         
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto text-white">
                    <li class="nav-item">
                        <asp:HyperLink ID="hlLogin" CssClass="nav-link" runat="server" NavigateUrl="~/login.aspx">Iniciar Sesión</asp:HyperLink>            
                    </li>
                    <li class="nav-item">
                        <asp:HyperLink ID="hlRegistro" CssClass="nav-link" runat="server" NavigateUrl="~/registro.aspx">Registrarse</asp:HyperLink>           
                    </li>                    
                </ul>
            </div>
        </nav>
        
        <div class="container">
            <div class="row">
                <div class="col-4"></div>
                <div class="col-4">
                    <div class="form-group">
                        <asp:Label CssClass="h1" ID="lblPregunta" runat="server" Text="Pregunta"></asp:Label>
                    </div>
                   <div class="form-group">
                        <asp:DropDownList CssClass="form-control" ID="ddlRespuesta" runat="server">
                            <asp:ListItem Text="Verdadero" Value="1" />
                            <asp:ListItem Text="Falso" Value="2" />
                            <asp:ListItem Text="Reportar" Value="3" />
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <asp:LinkButton ID="lkEnviar" CssClass="btn btn-primary" runat="server" OnClick="lkEnviar_Click">Contestar</asp:LinkButton>
                    </div>
                    
                </div>
            </div>
        </div>
    </form>
</body>
</html>
