﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="panel.aspx.cs" Inherits="Practica4.panel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>panel</title>
    <link href="Utelerias/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        
    <div class="container">
        <ul class="nav justify-content-end">
          <li class="nav-item">
              <asp:LinkButton ID="lkCerrar" runat="server" OnClick="lkCerrar_Click">Cerrar Sesión</asp:LinkButton>
          </li>          
        </ul>

        <div class="form-group">
            <asp:Label ID="Label1" AssociatedControlID="txtPregunta" runat="server" Text="Pregunta"></asp:Label>
            <asp:TextBox ID="txtPregunta" required ="required" CssClass="form-control" runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:DropDownList CssClass="form-control" ID="ddlRespuesta" runat="server">
                <asp:ListItem Text="Verdadero" />
                <asp:ListItem Text="Falso" />
            </asp:DropDownList>
        </div>
        <div class ="form-group">
            <asp:Button ID="btnAgregar" CssClass="btn btn-primary" Text="Agregar Pregunta" runat="server" OnClick="btnAgregar_Click" />
        </div>

        <h4>Seleccion</h4>
        <asp:Label ID="lblPregunta" runat="server" Text="No hay selección"></asp:Label>
        <asp:Label ID="lblID" runat="server" Text=""></asp:Label>
        <br />
        <asp:LinkButton ID="lkEliminar" CssClass="btn btn-danger" runat="server" OnClick="lkEliminar_Click">Eliminar pregunta</asp:LinkButton>
        <asp:LinkButton ID="lkDesactivar" CssClass="btn btn-warning" runat="server" OnClick="lkDesactivar_Click">Desactivar</asp:LinkButton>
        
        <h4>tus preguntas</h4>
        <asp:LinkButton ID="actualizar" runat="server" OnClick="actualizar_Click">Actualizar Tabla</asp:LinkButton>
        <asp:GridView ID="gvPreguntas" DataKeyNames="id_trivia" AutoGenerateColumns="false" OnSelectedIndexChanged="gvPreguntas_SelectedIndexChanged" CssClass="table table-hover table-responsive-lg" runat="server">
            <Columns>
                <asp:BoundField DataField="id_trivia" HeaderText="ID"/>
                <asp:BoundField DataField="trivia" HeaderText="Trvia"/>
                <asp:BoundField DataField="respuesta" HeaderText="Respuesta" />
                <asp:BoundField DataField="respuestasCorrectas" HeaderText="No. Correctas" />
                <asp:BoundField DataField="respuestasIncorrectas" HeaderText="No. Incorrectas"/>
                <asp:BoundField DataField="estatus" HeaderText="estatus" />
                <asp:BoundField DataField="reportes" HeaderText="reportes" />
                <asp:BoundField DataField="total" HeaderText="Total" />
                <asp:ButtonField ButtonType="Link" CommandName="Select" HeaderText="Opciones" Text="Seleccionar" />
            </Columns>
        </asp:GridView>
    </div>
    </form>
</body>
</html>
