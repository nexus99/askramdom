CREATE DATABASE practica4;
GO
USE practica4;
GO

CREATE TABLE Usuarios
(
	id_usuario		INT PRIMARY KEY IDENTITY(1,1),
	usuario			VARCHAR(250) NOT NULL,
	contrasena		VARCHAR(MAX) NOT NULL
);
GO

CREATE TABLE estatus
(
	id_estatus		INT PRIMARY KEY IDENTITY(1,1),
	estatus			VARCHAR(250) NOT NULL
);
GO

INSERT INTO estatus(estatus) VALUES ('activo'),('desactivado'),('reportado');
GO

CREATE TABLE respuestas
(
	id_respuesta	INT PRIMARY KEY IDENTITY(1,1),
	respuesta		VARCHAR(250) NOT NULL
);
GO

INSERT INTO respuestas(respuesta) VALUES ('Verdadero'),('Falso');
GO

CREATE TABLE trivias
(
	id_trivia				INT PRIMARY KEY IDENTITY(1,1),
	trivia					VARCHAR(600) NOT NULL,
	id_respuesta			INT FOREIGN KEY REFERENCES respuestas(id_respuesta) NOT NULL,
	respuestasCorrectas		INT DEFAULT(0) NOT NULL,
	respuestasIncorrectas	INT DEFAULT(0) NOT NULL,
	reportes				INT DEFAULT(0) NOT NULL,
	total					INT DEFAULT(0) NOT NULL,
	fecheRegistro			DATE DEFAULT(GETDATE()),
	id_estatus				INT DEFAULT(1) FOREIGN KEY REFERENCES estatus(id_estatus) NOT NULL,
	id_usuario				INT FOREIGN KEY REFERENCES usuarios(id_usuario) NOT NULL

);
GO

SELECT * FROM trivias T INNER JOIN respuestas R ON t.id_respuesta = R.id_respuesta INNER JOIN estatus E on e.id_estatus = t.id_estatus